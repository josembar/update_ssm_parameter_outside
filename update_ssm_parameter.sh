#!/bin/bash

aws ssm put-parameter \
--overwrite \
--name testvalue \
--value 'testvalue-changed'