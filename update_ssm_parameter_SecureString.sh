#!/bin/bash

aws ssm put-parameter \
  --overwrite \
  --name testvalue \
  --type SecureString \
  --value 'testvalue-changed'